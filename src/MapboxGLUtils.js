import axios from 'axios';

export default class MenuMapBoxGL {
    constructor(options) {
        options = (!options) ? {
            layers: {}
        } : options;
        this._layers = options.layers
    }

    onAdd(map) {
        this._map = map;
        let _this = this;

        this._mapLayers = this._map.getStyle().layers;
        this._mapLayerIds = this.getMapLayerIds(this._mapLayers);

        this._menu = document.createElement('div');
        this._menu.className = 'mapbox-gl-ls';
        this._menu.id = 'mapbox_gl_menu';

        this._button = document.createElement('button');
        this._button.title = 'Меню шарів';
        this._menu.appendChild(this._button);

        this._panel = document.createElement('div');
        this._panel.className = 'panel';
        this._panel.id = 'panel';
        this._menu.appendChild(this._panel);

        this._ul = document.createElement('ul');
        this._panel.appendChild(this._ul);

        if (this._layers.baseLayers !== undefined && this._layers.baseLayers.length) {
            this._li_base_group = document.createElement('li');
            this._li_base_group.className = 'group mapbox-gl-ls-base-group';
            this._ul.appendChild(this._li_base_group);

            this._vaseLabel = document.createElement('label');
            this._vaseLabel.innerText = 'Базові шари';
            this._li_base_group.appendChild(this._vaseLabel);

            this._ul_base_subgroup = document.createElement('ul');
            this._li_base_group.appendChild(this._ul_base_subgroup);

            for (let i = 0; i < this._layers.baseLayers.length; i++) {
                let layer = Object.keys(this._layers.baseLayers[i])[0];
                if (this._mapLayerIds.indexOf(layer) > -1) {
                    let checked = this.getMapLayerVisibility(this._mapLayers, this._mapLayerIds, layer);
                    let index = this._mapLayerIds.indexOf(layer);
                    let legend = this._layers.baseLayers[i][layer];
                    let input = this.createBaseLayerRow(layer, checked, index, legend);
                    this._ul_base_subgroup.appendChild(input);
                }
            }

            function setBaseLayerVisibility(checked, layer) {
                for (let i = 0; i < _this._layers['baseLayers'].length; i++) {
                    let lr = Object.keys(_this._layers['baseLayers'][i])[0];
                    (lr === layer) ?
                        _this._map.setLayoutProperty(lr, 'visibility', 'visible') :
                        _this._map.setLayoutProperty(lr, 'visibility', 'none');
                }
            }

            this._ul_base_subgroup.addEventListener("click", function (e) {
                if (e.target.id) {
                    setBaseLayerVisibility(e.target.checked, e.target.value);
                }
            });
        }

        if (this._layers.overlayLayers !== undefined && this._layers.overlayLayers.length) {

            for (let i = 0; i < this._layers.overlayLayers.length; i++) {
                let layerGroup = this._layers.overlayLayers[i];

                this._li_overlay_group = document.createElement('li');
                this._li_overlay_group.className = 'group mapbox-gl-ls-fold';
                this._li_overlay_group.onclick = function (e) {
                    if (e.target.id) {
                        setOverlayLayerVisibility(e.target.checked, e.target.name);
                    }
                };

                (layerGroup['groupExpand'] === true) ?
                    this._li_overlay_group.classList.add('mapbox-gl-ls-open') :
                    this._li_overlay_group.classList.add('mapbox-gl-ls-close');

                this._ul.appendChild(this._li_overlay_group);

                this._buttonGroup = document.createElement('button');
                this._buttonGroup.onclick = function (e) {
                    let elem = e.target.closest('.mapbox-gl-ls-fold');
                    elem.classList.toggle('mapbox-gl-ls-open');
                    elem.classList.toggle('mapbox-gl-ls-close');
                };
                this._li_overlay_group.appendChild(this._buttonGroup);
                this._labelGroup = document.createElement('label');
                this._labelGroup.onclick = function (e) {
                    let elem = e.target.closest('.mapbox-gl-ls-fold');
                    elem.classList.toggle('mapbox-gl-ls-open');
                    elem.classList.toggle('mapbox-gl-ls-close');
                };
                this._labelGroup.innerText = layerGroup['groupName'];
                this._li_overlay_group.appendChild(this._labelGroup);

                this._ul_overlay_subgroup = document.createElement('ul');
                this._ul_overlay_subgroup.className = 'subgroup-ku';
                this._li_overlay_group.appendChild(this._ul_overlay_subgroup);

                for (let i = 0; i < layerGroup['groupLayers'].length; i++) {
                    let layer = Object.keys(layerGroup['groupLayers'][i])[0];
                    if (this._mapLayerIds.indexOf(layer) > -1) {
                        let checked = this.getMapLayerVisibility(this._mapLayers, this._mapLayerIds, layer);
                        let index = this._mapLayerIds.indexOf(layer);
                        let legend = layerGroup['groupLayers'][i][layer];
                        let input = this.createOverlayLayerRow(layer, checked, index, legend);
                        this._ul_overlay_subgroup.appendChild(input);
                    }
                }
            }

            this._ul.appendChild(this._li_overlay_group);
        }

        function setOverlayLayerVisibility(checked, layer) {
            let visibility = (checked === true) ? 'visible' : 'none';
            _this._map.setLayoutProperty(layer, 'visibility', visibility);
        }

        this._button.addEventListener("mouseover", function (e) {
            let mapbox_gl_menu = document.getElementById('mapbox_gl_menu');
            mapbox_gl_menu.classList.add('shown');
        });

        this._panel.addEventListener("mouseleave", function (e) {
            let mapbox_gl_menu = document.getElementById('mapbox_gl_menu');
            mapbox_gl_menu.classList.remove('shown');
        });

        return this._menu;
    }

    onRemove(map) {
        this._map = map;
        this._menu.parentNode.removeChild(this._menu);
        this._map = undefined;
    }

    getMapLayerIds(layers) {
        return layers.reduce((array, layer) => {
            return [...array, layer.id]
        }, [])
    }

    getMapLayerVisibility(layers, ids, layer) {
        let index = ids.indexOf(layer);
        return (layers[index].layout.visibility === "visible");
    }

    createBaseLayerRow(layer, checked, index, legend) {
        let li = document.createElement("li");
        li.className = "layer"
        let input = document.createElement("input");
        input.name = "base";
        input.value = layer;
        input.type = "radio";
        input.id = "layer_" + index;
        input.style.cursor = "pointer";
        if (checked) input.checked = true;
        let label = document.createElement("label");
        label.innerText = layer;
        label.setAttribute("for", "layer_" + index);
        label.innerHTML = legend;
        label.style.cursor = "pointer";
        label.style.lineHeight = "24px";
        li.appendChild(input);
        li.appendChild(label);
        return li;
    }

    createOverlayLayerRow(layer, checked, index, legend) {
        let li = document.createElement("li");
        li.className = "checkbox"
        let input = document.createElement("input");
        input.name = layer;
        input.type = "checkbox";
        input.id = "layer_" + index;
        input.className = "layer";
        input.style.cursor = "pointer";
        if (checked) input.checked = true;
        let label = document.createElement("label");
        label.innerText = layer;
        label.setAttribute("for", "layer_" + index);
        label.innerHTML = legend;
        label.style.cursor = "pointer";
        label.style.lineHeight = "24px";
        li.appendChild(input);
        li.appendChild(label);
        return li;
    }
}
export async function getSourceData(dataUrl, checkDataActualityUrl = null, sessionStorageName, callback) {
    let storageData = sessionStorage.getItem(sessionStorageName);
    let storageDataParsed = JSON.parse(storageData);

    async function getLastModifyDate(checkDataActualityUrl, timeMark = '') {
        return axios.get(checkDataActualityUrl + '&time_mark=' + timeMark).then((response) => {
            return response.data;
        }).catch((e) => {
            console.log(e);
        });
    }

    async function loadSourceData(url, sessionStorageName) {
        return axios.get(url).then((response) => {
            sessionStorage.setItem(sessionStorageName, JSON.stringify(response.data));
            return response.data;
        }).catch((e) => {
            console.log(e);
        });
    }

    if (storageData !== null) {
        if (checkDataActualityUrl !== null) {
            let lastModifyDate = await getLastModifyDate(checkDataActualityUrl, storageDataParsed.time_mark);

            if (lastModifyDate == null) {
                delete storageDataParsed.time_mark;
                return storageDataParsed;
            }
        }
    }

    return await loadSourceData(dataUrl, sessionStorageName);
}


